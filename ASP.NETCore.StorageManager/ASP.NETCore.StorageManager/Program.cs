using ASP.NETCore.StorageManager.Extensions;
using CYW.CoreLibrary.Notification.MQTT;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;

namespace ASP.NETCore.StorageManager
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                if (Log.Logger == null || Log.Logger.GetType().Name == "SilentLogger")
                {
                    // Loading configuration or Serilog failed.
                    // This will create a logger that can be captured by Azure logger.
                    // To enable Azure logger, in Azure Portal:
                    // 1. Go to WebApp
                    // 2. App Service logs
                    // 3. Enable "Application Logging (Filesystem)", "Application Logging (Filesystem)" and "Detailed error messages"
                    // 4. Set Retention Period (Days) to 10 or similar value
                    // 5. Save settings
                    // 6. Under Overview, restart web app
                    // 7. Go to Log Stream and observe the logs
                    Log.Logger = new LoggerConfiguration()
#if DEBUG
                        .MinimumLevel.Debug()
                        .WriteTo.Console()
#endif
#if RELEASE
                        .WriteTo.File(path: ".\\Serilogs\\SerilogSample.log", outputTemplate: "{Timestamp:HH:mm:ss} [{SourceContext}] [{Level:u3}] {Message}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
#endif
                        .CreateLogger();
                }

                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder
                .UseSerilog((hostingContext, loggerConfiguration) =>
                {
                    if (hostingContext.HostingEnvironment.IsProduction())
                    {
                        loggerConfiguration
                        .MinimumLevel.Information()
                        .Enrich.FromLogContext()
                        .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Debug)
                        .WriteTo.Map(logevent => logevent, (logevent, configure) =>
                        {
                            string filename = logevent.Properties["SourceContext"].ToString().Replace('\"', ' ').Trim();
                            configure.File($".\\Serilogs\\{filename}-{logevent.Level}-.log", outputTemplate: "{Timestamp:HH:mm:ss} [{Level:u3}] {Message}{NewLine}{Exception}", rollOnFileSizeLimit: true, shared: true);
                        });
                    }
                    else
                    {
                        loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration);
                    }
                    //.Enrich.With(new RemovePropertiesEnricher())
                    //.Enrich.WithProperty("ApplicationName", typeof(Program).Assembly.GetName().Name);
                    //.Enrich.WithProperty("Environment", hostingContext.HostingEnvironment);
#if DEBUG
                    //.WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Debug)
                    //.MinimumLevel.Debug();
                    // Used to filter out potentially bad data due debugging.
                    // Very useful when doing Seq dashboards and want to remove logs under debugging session.
                    //loggerConfiguration.Enrich.WithProperty("DebuggerAttached", Debugger.IsAttached);
#endif
#if RELEASE
                    //.WriteTo.File(path: ".\\Serilogs\\SerilogSample.log", outputTemplate: "{Timestamp:HH:mm:ss} [{SourceContext}] [{Level:u3}] {Message}{NewLine}{Exception}", rollingInterval: RollingInterval.Day);
#endif
                })
                 .UseNotification((hostingContext, configure) =>
                 {
                     MqttClientOption option = hostingContext.Configuration.GetSection("NotifycationSettings:MQTT").Get<MqttClientOption>() ?? throw new ApplicationException();
                     configure.Channel.UseMQTT(option);
                 })
                //.UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>();
            });
    }
}
