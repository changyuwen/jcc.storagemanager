﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StorageManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCore.StorageManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        public StorageController()
        {

        }
        [HttpGet]
        public ActionResult<AMHS> GetAMHS()
        {
            return MqttHostService.AMHS;
        }
        [HttpPut]
        public ActionResult<AMHS> PutCurrent([FromBody] int value)
        {
            MqttHostService.AMHS.Current++;
            return Accepted(MqttHostService.AMHS);
        }

        //[HttpPost]
        //[Consumes("text/plain")]
        //[ApiConventionMethod(typeof(DefaultApiConventions), nameof(DefaultApiConventions.Create))]
        //public ActionResult<string> Post([FromBody] string value)
        //{
        //    //MqttHostService.MessageList.Add(value);
        //    return CreatedAtAction("Post", value);
        //}
    }
}
