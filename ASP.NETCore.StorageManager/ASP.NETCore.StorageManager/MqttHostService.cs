﻿using CYW.CoreLibrary.MQTT.Client.WebSocketClient;
using CYW.CoreLibrary.MQTT.Protocal;
using CYW.CoreLibrary.Notification.MQTT;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using StorageManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ASP.NETCore.StorageManager
{
    public class MqttHostService : IHostedService
    {
        public MQTTClient MQTTClient { get; }
        private MqttClientOption MqttClientOption { get; }
        public static readonly AMHS AMHS = new();
        //public static readonly List<string> MessageList = new();
        public MqttHostService(IOptions<MqttClientOption> option)
        {
            MqttClientOption = option.Value ?? throw new ArgumentNullException(nameof(option));
            MQTTClient = MQTTClient.CreateClient(MqttClientOption.BrokerHost, op =>
            {
                op.ClientID = MqttClientOption.ClientID;
                op.Username = MqttClientOption.Username;
                op.ProtocolVersion = ProtocolVersion.V311;
                op.WaitingTime = MqttClientOption.WaitingTime;
            });
            MQTTClient.UseKeepAlive();
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await MQTTClient.SubscribeAsync("AMHS", message =>
            {
                var obj = JsonSerializer.Deserialize<AMHS>(message, new JsonSerializerOptions(JsonSerializerDefaults.Web));
                AMHS.CommunicationState = obj.CommunicationState;
                AMHS.CtrlState = obj.CtrlState;
                AMHS.PortState = obj.PortState;
                AMHS.SCState = obj.SCState;
                AMHS.Title = obj.Title;
                AMHS.Current = obj.Current;
                return Task.CompletedTask;
            });
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return MQTTClient.Disconnect();
        }
    }
}
