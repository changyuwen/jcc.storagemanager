﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace ASP.NETCore.StorageManager
{
    internal class JwtAuthentication
    {
        public string SecurityKey { get; set; }
        public int ExpiredTime { get; set; } = 5;
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public bool ValidateIssuer => Issuer.Length > 0;
        public bool ValidateAudience => Audience.Length > 0;
        public bool ValidateIssuerSigningKey => !(SymmetricSecurityKey is null);
        public SymmetricSecurityKey SymmetricSecurityKey => SecurityKey.Length == 32 ? new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey)) : null;
        public TimeSpan ClockSkew => TimeSpan.FromHours(ExpiredTime);
        public SigningCredentials SigningCredentials => new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha256);
    }
}