﻿using CYW.CoreLibrary.Notification;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCore.StorageManager.Extensions
{
    public static class WebHostBuilderExtensions
    {
        public static IWebHostBuilder UseNotification(
           this IWebHostBuilder builder,
           Action<WebHostBuilderContext, NotificationFactory> configure)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (configure == null) throw new ArgumentNullException(nameof(configure));

            builder.ConfigureServices((context, collection) =>
            {
                var notificationFactory = new NotificationFactory();
                configure(context, notificationFactory);
                NotifyStation.Hub = notificationFactory.AggregateHub();
                collection.AddSingleton(NotifyStation.Hub);
            });
            return builder;
        }
        public static IHostBuilder UseNotification(
            this IHostBuilder builder,
            Action<HostBuilderContext, NotificationFactory> configure)
        {
            if (builder == null) throw new ArgumentNullException(nameof(builder));
            if (configure == null) throw new ArgumentNullException(nameof(configure));

            builder.ConfigureServices((context, collection) =>
            {
                var notificationFactory = new NotificationFactory();
                configure(context, notificationFactory);
                NotifyStation.Hub = notificationFactory.AggregateHub();
                collection.AddSingleton(NotifyStation.Hub);
            });
            return builder;
        }
    }
}
