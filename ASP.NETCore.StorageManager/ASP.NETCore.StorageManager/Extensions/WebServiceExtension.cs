﻿using CYW.CoreLibrary.Notification.MQTT;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ASP.NETCore.StorageManager.Extensions
{
    public static class WebServiceExtension
    {
        /// <summary>
        /// Add Swagger to display Web-API and  API document.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, SwaggerDocumentationOptions options)
        {
            services.AddControllers();
            return services.AddSwaggerDocumentation(options.Version, options.Title, options.Description, options.UseBearerToken, options.ApiContact, options.ApiLicense);
        }
        /// <summary>
        /// Add Swagger to display Web-API and  API document.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="version"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="useAuthorize"></param>
        /// <param name="apiContact"></param>
        /// <param name="license"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, string version, string title, string description, bool useAuthorize = false, OpenApiContact apiContact = null, OpenApiLicense license = null)
        {
            return services.AddSwaggerGen(options =>
            {

                options.SwaggerDoc($"{typeof(Startup).Assembly.GetName().Name}",
                    new OpenApiInfo
                    {
                        Version = version,
                        Title = title,
                        Description = description,
                        Contact = apiContact,
                        License = license
                    });
                if (useAuthorize)
                {
                    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                    });
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference()
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme
                                }
                            }, Array.Empty<string>() }
                    });
                }

                //Determine base path for the application and set the comments path for the swagger json and ui. 
                if (File.Exists(Path.Combine(AppContext.BaseDirectory, $"{typeof(Startup).Assembly.GetName().Name}.xml")))
                    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{typeof(Startup).Assembly.GetName().Name}.xml"), true);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="prefix"></param>
        /// <param name="setupAction"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app, string prefix = "", Action<SwaggerUIOptions> setupAction = null)
        {
            if (prefix.Length > 0)
            {
                prefix = prefix.Trim('/');
                prefix = "/" + prefix;
            }
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint($"{prefix}/swagger/{typeof(Startup).Assembly.GetName().Name}/swagger.json", "Versioned API v1.0");
                options.DocumentTitle = "Title Documentation";
                options.DocExpansion(DocExpansion.None);

                setupAction?.Invoke(options);
            });

            return app;
        }
    }
    
}
