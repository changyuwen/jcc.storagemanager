﻿using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCore.StorageManager.Extensions
{
    public class SwaggerDocumentationOptions
    {
        public string Version { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public bool UseBearerToken { get; set; } = false;
        public OpenApiLicense ApiLicense { get; set; } = new OpenApiLicense();
        public OpenApiContact ApiContact { get; set; } = new OpenApiContact();

    }
}
