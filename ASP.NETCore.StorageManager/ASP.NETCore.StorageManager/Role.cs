﻿
namespace ASP.NETCore.StorageManager
{
    internal class Role
    {
        public string Name { get; set; }
        public bool IsDefault { get; set; }

        public const string PolicyA = "PolicyA";
        public const string PolicyB = "PolicyB";
        public const string SystemOwner = "SystemOwner";
        public const string SystemAdmin = "SystemAdmin";
        public const string Administrator = "Administrator";
        //public IEnumerable<User> Users { get; set; } = new List<User>();
    }
}