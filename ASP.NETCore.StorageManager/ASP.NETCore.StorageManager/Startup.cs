using ASP.NETCore.StorageManager.Extensions;
using CYW.CoreLibrary.Notification;
using CYW.CoreLibrary.Notification.MQTT;
using Microsoft.AspNet.OData.Formatter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ASP.NETCore.StorageManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        internal JwtAuthentication JwtAuthentication => Configuration.GetSection("JwtAuthentication").Get<JwtAuthentication>() ?? throw new ApplicationException();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MqttClientOption>(Configuration.GetSection("MQTTClientSettings"));
            services.AddSingleton<IHostedService, MqttHostService>();
            services.AddCors(options =>
            {
                // 'CorsPolicy' is Policy name by yourself
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyOrigin() //.WithOrigins("")
                    .WithMethods("GET", "PUT", "POST", "DELETE") //AllowSpecificMethods;
                    .WithHeaders("Accept", "Content-type", "Origin", "X-Custom-Header", "Authorization")  //AllowSpecificHeaders;
                    );
            });
            services.AddAuthorization(options =>
            {
                // Assign policy to building a combination of roles to keep a consistent authorization.
                // see https://docs.microsoft.com/en-us/aspnet/core/security/authorization/policies?view=aspnetcore-3.1
                options.AddPolicy(Role.PolicyA,
                     policy => policy.RequireRole(Role.Administrator, Role.SystemAdmin, Role.SystemOwner));
                options.AddPolicy(Role.PolicyB,
                    policy => policy.RequireRole(Role.Administrator, Role.SystemOwner));
            });
            //Using Json Web Token for validate authentication .
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            //this.Logger?.LogError("OnAuthenticationFailed: ", context.Exception.Message);                        
                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {
                            //this.Logger?.LogError("OnTokenValidated: ", context.SecurityToken);                          
                            return Task.CompletedTask;
                        }
                    };
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // Clock skew compensates for server time drift.
                        // We recommend 5 minutes or less:
                        ClockSkew = JwtAuthentication.ClockSkew,
                        // Specify the key used to sign the token:
                        ValidateIssuerSigningKey = JwtAuthentication.ValidateIssuerSigningKey,
                        IssuerSigningKey = JwtAuthentication.SymmetricSecurityKey,
                        RequireSignedTokens = true,
                        // Ensure the token hasn't expired:
                        RequireExpirationTime = true,
                        ValidateLifetime = true,
                        // Ensure the token audience matches our audience value (default true):
                        ValidateAudience = JwtAuthentication.ValidateAudience,
                        ValidAudience = JwtAuthentication.Audience,
                        // Ensure the token was issued by a trusted authorization server (default true):
                        ValidateIssuer = JwtAuthentication.ValidateIssuer,
                        ValidIssuer = JwtAuthentication.Issuer
                    };
                });
            services.AddSwaggerDocumentation(new SwaggerDocumentationOptions()
            {
                Version = "v1",
                Description = "This is an ASP.Net Core Web Api.",
                Title = "Web API template.",
                UseBearerToken = true
            });
            //services.AddControllers(o => o.InputFormatters.Insert(o.InputFormatters.Count, new TextPlainInputFormatter()));

            services.AddMvcCore(options =>
            {
                options.InputFormatters.Insert(options.InputFormatters.Count, new TextPlainInputFormatter());
                foreach (ODataOutputFormatter outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(foramtter => foramtter.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/odata"));
                }
                foreach (ODataInputFormatter inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(foramtter => foramtter.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/odata"));
                }
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp";
            });
            services.AddLogging();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            if (env.IsProduction())
            {
                app.UseExceptionHandler("/api/error");
                app.UseHsts();
                app.UseHttpsRedirection();
            }
            app.Use(async (context, next) =>
            {
                //Do work that doesn't write to the Response.                       

                if (HttpMethods.IsPatch(context.Request.Method) || HttpMethods.IsPost(context.Request.Method) || HttpMethods.IsPut(context.Request.Method))
                {
                    Stream originalBodyStream = context.Response.Body;
                    using MemoryStream fakeResponseBody = new();
                    context.Response.Body = fakeResponseBody;
                    await next.Invoke();
                    if (context.Response.StatusCode == 201 || context.Response.StatusCode == 202)
                    {
                        fakeResponseBody.Seek(0, SeekOrigin.Begin);
                        string text = await new StreamReader(fakeResponseBody).ReadToEndAsync();
                        string sub = $"{context.Request.Method.ToUpper()}/{context.Request.Path.Value.Split('/').Last()}";
                        if (sub.Length > 0 && text.Length > 0)
                        {
                            await NotifyStation.Hub.SendMessageAsync(sub, text, string.Empty);
                        }
                    }
                    fakeResponseBody.Seek(0, SeekOrigin.Begin);
                    await fakeResponseBody.CopyToAsync(originalBodyStream);
                }
                else
                {
                    await next.Invoke();
                }
                //Do logging or other work that doesn't write to the Response.

            });
            // This will make the HTTP requests log as rich logs instead of plain text.
            app.UseSerilogRequestLogging();

            app.UseSwaggerDocumentation();

            //app.UseStaticFiles();
            // Re-write path only. / to /index.html
            app.UseDefaultFiles();

            // Start serve files from wwwroot (Must be after UseDefaultFiles obviously.)
            app.UseSpaStaticFiles();

            app.UseCors("CorsPolicy");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapDefaultControllerRoute();
                endpoints.MapControllers();
                //endpoints.Select().Filter().OrderBy().Count().MaxTop(100);
                //endpoints.MapODataRoute("odata", "odata", GetEdmModel());
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";                
            });
        }
    }
}
