﻿using CYW.CoreLibrary.MQTT.Client.WebSocketClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        private readonly JM.CallBack TFunction;
        public UIntPtr station;
        private readonly string DestIP = "127.0.0.1";
        private readonly string DestPort = "5003";
        private readonly string Port = "5002";
        public SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(0);
        public Queue<string> stringQueue = new Queue<string>();
        public CancellationTokenSource tokenSource = new CancellationTokenSource();
       
        public MqttClientOption Option { get; } = new MqttClientOption() 
        { 
            BrokerHost = "ws://localhost:80/mqtt", 
            ClientID = "clientId-DEV", 
            Username = "Username", 
            WaitingTime = 60 
        };
        public MQTTClient client;
        public Form1()
        {
            InitializeComponent();
            TFunction = CB;
            JM.Initialize();
            station = JM.StartStation("APP1", TFunction, 0, ushort.Parse(textBox1.Text));
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            if (backgroundWorker1.IsBusy != true)
            {
                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void CB(uint Station, uint type, uint ticket, ref byte[] s, uint length, uint sb, uint ip, ushort port)
        {
            string p = Encoding.Default.GetString(s, 0, s.Length);
            if (p.Length > 0)
            {
                richTextBox1.Text = p;
                stringQueue.Enqueue(p);
                SemaphoreSlim.Release();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            uint ticket = 1;
            string str = "123";
            int state = JM.Send(station, str, (uint)str.Length, ToAddrInt(DestIP), ushort.Parse(textBox2.Text), ref ticket);
        }
        private static uint ToAddrInt(string addr)
        {
            byte[] ipBytes = System.Net.IPAddress.Parse(addr).GetAddressBytes();
            uint ip = (uint)ipBytes[3] << 24;
            ip += (uint)ipBytes[2] << 16;
            ip += (uint)ipBytes[1] << 8;
            ip += ipBytes[0];
            return ip;
        }

        private async void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //await client.Disconnect();
            backgroundWorker1.CancelAsync();
            SemaphoreSlim.Release();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            //client = await MQTTClient.CreateClientAsync("ws://localhost:80/mqtt", option =>
            //{
            //    option.ClientID = "ClientID";
            //    option.Username = "Username";
            //    option.ProtocolVersion = CYW.CoreLibrary.MQTT.Protocal.ProtocolVersion.V311;
            //    option.WaitingTime = 60;
            //    //option.Provider.OnHandledPingResp = context =>
            //    //{
            //    //    Console.WriteLine(context.PingRespTime);
            //    //    return Task.CompletedTask;
            //    //};
            //});

            //client.UseKeepAlive();
            //await client.SubscribeAsync("PUT/Storage", message =>
            //{
            //    uint ticket = 1;
            //    int state = JM.Send(station, message, (uint)message.Length, ToAddrInt(DestIP), ushort.Parse(textBox2.Text), ref ticket);
            //    return Task.CompletedTask;
            //});
        }

        private async void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!tokenSource.IsCancellationRequested)
            {
                await SemaphoreSlim.WaitAsync(tokenSource.Token);
                try
                {
                    while (stringQueue.Count > 0)
                    {
                        await client.PublishAsync("AMHS", stringQueue.Dequeue());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }               
            }         
        }
    }

    public class MqttClientOption : WebSocketClientOption
    {
        public string BrokerHost { get; set; }
    }
}
