//using CYW.CoreLibrary.MQTT.Client.WebSocketClient;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServiceTemplate1
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> Logger;
        public MqttClientOption Option { get; }
        //public MQTTClient MQTTClient { get; }
        private readonly JM.CallBack TFunction;
        public UIntPtr station;
        private readonly string DestIP = "127.0.0.1";
        private string DestPort = "5003";
        private string Port = "5002";
        public Worker(ILogger<Worker> logger, IOptions<MqttClientOption> option)
        {
            Logger = logger ?? throw new ArgumentNullException(nameof(option));
            Option = option is not null ? option.Value : throw new ArgumentNullException(nameof(option));

            //MQTTClient = MQTTClient.CreateClient(Option.BrokerHost, option =>
            //{
            //    option.ClientID = Option.ClientID;
            //    option.Username = Option.Username;
            //    option.ProtocolVersion = CYW.CoreLibrary.MQTT.Protocal.ProtocolVersion.V311;
            //    option.WaitingTime = Option.WaitingTime;
            //    option.Provider.OnHandledPingResp = context =>
            //    {
            //        Logger.LogDebug("Ping.");
            //        return Task.CompletedTask;
            //    };
            //});
            //MQTTClient.UseKeepAlive();
            TFunction = CB;
            JM.Initialize();
            station = JM.StartStation("APP1", TFunction, 0, ushort.Parse(Port));
        }
        private void CB(uint Station, uint type, uint ticket, ref byte[] s, uint length, uint sb, uint ip, ushort port)
        {
            string p = Encoding.Default.GetString(s, 0, s.Length);          
           

        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {            
            //await MQTTClient.SubscribeAsync("AAA", (message) =>
            //{
            //    // Do some async action.
            //    Console.WriteLine(message);
            //    return Task.CompletedTask;
            //});
            //await Task.Delay(1000, stoppingToken);
            //await MQTTClient.PublishAsync("AAA", "AAA");
            Logger.LogInformation("HostService execute StartAsync completed.");
            return Task.CompletedTask;
        }
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("HostService execute StopAsync completed");
            return Task.CompletedTask;
            //return MQTTClient.Disconnect();
        }
    }
}
