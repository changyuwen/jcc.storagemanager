using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServiceTemplate1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                // Log.Logger will likely be internal type "Serilog.Core.Pipeline.SilentLogger".
                if (Log.Logger == null || Log.Logger.GetType().Name == "SilentLogger")
                {
                    // Loading configuration or Serilog failed.
                    // This will create a logger that can be captured by Azure logger.
                    // To enable Azure logger, in Azure Portal:
                    // 1. Go to WebApp
                    // 2. App Service logs
                    // 3. Enable "Application Logging (Filesystem)", "Application Logging (Filesystem)" and "Detailed error messages"
                    // 4. Set Retention Period (Days) to 10 or similar value
                    // 5. Save settings
                    // 6. Under Overview, restart web app
                    // 7. Go to Log Stream and observe the logs
                    Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                         .WriteTo.File(path: $"{AppContext.BaseDirectory}\\Serilogs\\Program.log", outputTemplate: "{Timestamp:HH:mm:ss} [{SourceContext}] [{Level:u3}] {Message}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
                        .CreateLogger();
                }

                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
             //.ConfigureAppConfiguration((context, builder) =>
             //{
             //    builder.SetBasePath(Directory.GetCurrentDirectory())
             //    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
             //    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ENVIRONMENT")}.json", optional: true);
             //    .AddEnvironmentVariables();
             //})
             .UseWindowsService()
            .ConfigureServices((hostContext, services) =>
            {
                services.Configure<MqttClientOption>(hostContext.Configuration.GetSection("MQTTClientSettings"));
                services.AddHostedService<Worker>();
            })
            .UseSerilog((hostingContext, loggerConfiguration) =>
            {
                loggerConfiguration
                    .ReadFrom.Configuration(hostingContext.Configuration, sectionName: "Serilog") // 可以自訂sectionName，預設是Serilog；
                    .Enrich.FromLogContext()
                    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Verbose)
                    .WriteTo.Map(logevent => logevent,
                    (logevent, configure) =>
                    {
                        string filename = logevent.Properties["SourceContext"].ToString().Replace('\"', ' ').Trim();
                        configure.File($"{AppContext.BaseDirectory}\\Serilogs\\{filename}-{logevent.Level}.log"
                            , outputTemplate: "{Timestamp:HH:mm:ss} [{Level:u3}] {Message}{NewLine}{Exception}"
                            , restrictedToMinimumLevel: LogEventLevel.Information
                            , rollingInterval: RollingInterval.Day
                            , rollOnFileSizeLimit: true
                            , shared: true);
                    });
            });
    }
}
