using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

class JM
{
    public delegate void CallBack(
          [MarshalAs(UnmanagedType.U4)] uint Station,
          [MarshalAs(UnmanagedType.U4)] uint type,
          [MarshalAs(UnmanagedType.U4)] uint ticket,
         [MarshalAs(UnmanagedType.SafeArray, SafeArraySubType = System.Runtime.InteropServices.VarEnum.VT_UI1)] ref byte[] s,
          [MarshalAs(UnmanagedType.U4)] uint length,
          [MarshalAs(UnmanagedType.U4)] uint sb,
          [MarshalAs(UnmanagedType.U4)] uint ip,
          [MarshalAs(UnmanagedType.U2)] ushort port
          );

    [DllImport("JM.dll", EntryPoint = "JM_CS_Initialize", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void Initialize();

    [DllImport("JM.dll", EntryPoint = "JM_CS_DeInitialize", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void DeInitialize();

    [DllImport("JM.dll", EntryPoint = "JM_CS_StartStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern UIntPtr StartStation(string s, CallBack cb, uint ip, ushort port);

    [DllImport("JM.dll", EntryPoint = "JM_CS_StopStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void StopStation(uint Station);

    [DllImport("JM.dll", EntryPoint = "JMS_VB_Send", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int Send(UIntPtr Station, [MarshalAs(UnmanagedType.LPStr)] string script, uint len, uint ip, ushort port, ref uint ticket);

    [DllImport("JM.dll", EntryPoint = "JMS_CS_SendRequest", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int SendRequest(uint Station, [MarshalAs(UnmanagedType.AnsiBStr)] string script, uint ip, ushort port, ref uint ticket);

    [DllImport("JM.dll", EntryPoint = "JMS_CS_SendReply", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int SendReply(uint Station, [MarshalAs(UnmanagedType.AnsiBStr)] string script, uint ip, ushort port, uint sb, ref uint ticket);

    [DllImport("JM.dll", EntryPoint = "JMS_CS_Version", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    [return: MarshalAs(UnmanagedType.LPStr)]
    public static extern string Version();

    [DllImport("JM.dll", EntryPoint = "JMS_CS_SetTimer", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int SetTimer(uint Station, [MarshalAs(UnmanagedType.AnsiBStr)] string script, int type, uint ms, ref uint ticket);

    [DllImport("JM.dll", EntryPoint = "JMS_CS_CancelTimer", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int CancelTimer(uint Station, uint ticket);

    [DllImport("JM.dll", EntryPoint = "JM_CS_GetName", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr GetNameEx(uint station);

    public static string GetName(uint station)
    {
        return Marshal.PtrToStringAnsi(GetNameEx(station));
    }

}
