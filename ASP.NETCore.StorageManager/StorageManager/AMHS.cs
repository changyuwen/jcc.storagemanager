﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StorageManager
{
    public class AMHS
    {
        public string Title { get; set; } = string.Empty;
        public CtrlState CtrlState { get; set; } = CtrlState.None;
        public SCState SCState { get; set; } = SCState.None;
        public CommunicationState CommunicationState { get; set; } = CommunicationState.None;
        public PortState PortState { get; set; } = PortState.Up;
        public int Current { get; set; } = 0;
    }

    public enum CtrlState
    {
        Equipment_Off_Line,
        Attempt_On_Line,
        Host_Off_Line,
        Local,
        Remote,
        Crane_Down,
        Some_Port_Down,
        Control_Shul_Down,
        Not_Available,
        None
    }
    public enum SCState
    {
        SCInit,
        Paused,
        Auto,
        Pausing,
        None
    }
    public enum CommunicationState
    {
        Communication,
        None_Communication,
        None
    }
    public enum PortState
    {
        Up,
        Down
    }
}
