export const environment = {
  production: true,
  mqtt: {
    Host: `${window.location.hostname}`,
    Port: '83',
    ClientID: `clientId-${Date.now().toString()}`,   
  },
  api:{
    baseAddress: `${window.location.origin}/demo`,
    Endpoint: 'api/Storage'
  }
};
