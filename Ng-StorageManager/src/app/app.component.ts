import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { IMqttMessage, IMqttServiceOptions, IOnConnectEvent, MqttConnectionState, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StkOptions } from './component/stk/stk.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Ng-StorageManager';
  deleteType: string = "toggle";
  allowDeleting: boolean = false;
  topic: string = "POST/Storage";
  current: number = 10;
  ctrl_state: string = 'none';
  stkOptions: StkOptions | undefined;
  MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
    connectOnCreate: false,
    hostname: environment.mqtt.Host,
    port: window.location.protocol == 'https:' ? 443 : 80,
    protocol: window.location.protocol == 'https:' ? 'wss' : "ws",
    path: '/mqtt',
    clientId: environment.mqtt.ClientID
  };

  private subscription: Subscription | undefined;
  private subscription2: Subscription | undefined;
  constructor(private _mqttService: MqttService, private httpclient: HttpClient) {
    console.log(this.MQTT_SERVICE_OPTIONS);
    this._mqttService.state.subscribe((state: MqttConnectionState) => {
      // if (MqttConnectionState.CONNECTED == state) {

      // }
      console.log(state);
    });
    this.httpclient.get<StkOptions>(`${environment.api.baseAddress}/${environment.api.Endpoint}`).subscribe(options => {
      this.stkOptions = options;
    });

    this._mqttService.connect(this.MQTT_SERVICE_OPTIONS);
    console.log(this._mqttService.clientId);
  }
  ngOnInit(): void {
    console.log('ngOnInit');

    this.subscription = this._mqttService.observe('AMHS').subscribe((message: IMqttMessage) => {
      let pm = message.payload.toString();
      this.stkOptions = JSON.parse(pm);
    });
    this.subscription2 = this._mqttService.observe('PUT/Storage').subscribe((message: IMqttMessage) => {
      let pm = message.payload.toString();
      this.stkOptions = JSON.parse(pm);
    });
  }
  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
    this._mqttService.disconnect();
  }
  async click(event: any): Promise<void> {
    let options = await this.httpclient.put<StkOptions>(`${environment.api.baseAddress}/${environment.api.Endpoint}`, 1).toPromise();
  }
}
