import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrackComponent } from './component/track/track/track.component';
import { ChipComponent } from './component/chip/chip.component';
import { ArrowComponent } from './component/arrow/arrow.component';
import { ArrowItemComponent } from './component/arrowitem/arrowitem.component';
import { HttpClientModule } from '@angular/common/http';

import { IMqttServiceOptions, MqttModule } from 'ngx-mqtt';
import { DxButtonModule, DxListModule } from 'devextreme-angular';
import { environment } from 'src/environments/environment';
import { StkComponent } from './component/stk/stk.component';

export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  connectOnCreate: false,
  hostname: environment.mqtt.Host,
  port: window.location.protocol == 'https:' ? 443 : 80,
  protocol: window.location.protocol == 'https:' ? 'wss' : "ws",
  path: '/mqtt'
};

@NgModule({
  declarations: [
    AppComponent,
    StkComponent,
    TrackComponent,
    ChipComponent,
    ArrowComponent,
    ArrowItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS),
    DxListModule,
    DxButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
