import { Component, Input, OnInit } from '@angular/core';
export enum CtrlState {
  Equipment_Off_Line,
  Attempt_On_Line,
  Host_Off_Line,
  Local,
  Remote,
  Crane_Down,
  Some_Port_Down,
  Control_Shul_Down,
  Not_Available,
  None
}
export enum SCState {
  SCInit,
  Paused,
  Auto,
  Pausing,
  None
}
export enum CommunicationState {
  Communication,
  None_Communication,
  None
}
export enum PortState {
  Up,
  Down
}
export class StkOptions {
  ctrlState: CtrlState = CtrlState.None;
  scState: SCState = SCState.None;
  communicationState: CommunicationState = CommunicationState.None;
  portState: PortState = PortState.Down
  current: number = 0;
}

@Component({
  selector: 'app-stk',
  templateUrl: './stk.component.html',
  styleUrls: ['./stk.component.scss']
})
export class StkComponent implements OnInit {
  // @Input('current')
  // current: number = 0;
  @Input('max')
  max: number = 132;
  @Input('move')
  move: number = 0;
  @Input('title')
  title: string = '';
  @Input('ctrl_state')
  ctrl_state: CtrlState | undefined;
  @Input('options')
  options: StkOptions | undefined;

  constructor() { }

  ngOnInit(): void {
  }
  get current(): number {
    if (this.options) {
      return this.options.current;
    }
    return 0;
  }
  get rate(): string {
    if (this.options) {
      return ((this.options.current / this.max) * 100).toFixed(2);
    }
    return "0";
  }
  get percentage(): string {
    if (this.options) {
      let rate = (this.options.current / this.max) * 100;
      if (rate > 60) {
        return "red";
      }
      else if (rate > 40) {
        return "skyblue";
      }
    }
    return "springgreen";
  }

  get Ctrl(): string {
    if (this.ctrl_state) {
      return CtrlState[this.ctrl_state];
    }
    if (this.options) {
      return CtrlState[this.options.ctrlState];
    }
    return CtrlState[CtrlState.None];
  }

  get Port(): string {
    if (this.options) {
      return PortState[this.options.portState];
    }
    return PortState[PortState.Up];
  }
  get Sc(): string {
    if (this.options) {
      return SCState[this.options.scState];
    }
    return SCState[SCState.None];
  }
  get Cs(): string {
    if (this.options) {
      return CommunicationState[this.options.communicationState];
    }
    return CommunicationState[CommunicationState.None];
  }
}
