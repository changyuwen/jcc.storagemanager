import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {
  @Input('width')
  width: number | undefined;
  @Input('hight')
  hight: number | undefined;
  @Input('tl')
  top_left:number | undefined;
  @Input('tr')
  top_right:number | undefined;
  @Input('br')
  bottom_right:number | undefined;
  @Input('bl')
  bottom_left:number | undefined;
  constructor() { }

  ngOnInit(): void {
  }

}
