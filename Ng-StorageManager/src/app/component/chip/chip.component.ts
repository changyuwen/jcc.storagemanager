import { AfterContentInit, AfterViewInit, Component, ContentChild, ContentChildren, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ArrowComponent } from '../arrow/arrow.component';
import { ArrowItemComponent, IArrowItemOperation } from '../arrowitem/arrowitem.component';



@Component({
  selector: 'app-chip',
  templateUrl: './chip.component.html',
  styleUrls: ['./chip.component.scss']
})
export class ChipComponent implements OnInit, AfterContentInit {
  arrows: IArrowItemOperation[] = new Array();
  @Input('mark')
  mark: string = '';
  @Output('onclick')
  onclickEvent = new EventEmitter<string>();
  @Input('M01')
  m01: boolean = false;
  @Input('M02')
  m02: boolean = false;
  @ContentChildren(ArrowItemComponent)
  jokeContentChildren: QueryList<ArrowItemComponent> | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    this.jokeContentChildren?.toArray().forEach(obj => {
      this.arrows.push(obj)
      // this.arrows.push({ tag: obj.tag, width: obj.width, height: obj.height, deg: obj.deg, enable: obj.enable, side: obj.side, distance: obj.distance });
    });
  }

  getStyles(operation: IArrowItemOperation) {
    if (operation.side == 'Up') {
      return {
        top: `-${operation.height}px`,
        left: `${operation.distance}px`
      };
    } else if (operation.side == 'Down') {
      return {
        bottom: `-${operation.height}px`,
        left: `${operation.distance}px`
      };
    }
    return null;
  }
}
