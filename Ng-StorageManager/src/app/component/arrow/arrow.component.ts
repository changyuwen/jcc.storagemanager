import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface IArrowOperations {
  tag?: string;
  width: number;
  height: number;
  deg: number;
  enable: boolean;
}

@Component({
  selector: 'app-arrow',
  templateUrl: './arrow.component.html',
  styleUrls: ['./arrow.component.scss']
})
export class ArrowComponent implements OnInit {
  @Input('tag')
  tag: string | undefined;
  @Input('width')
  width: number | undefined;
  @Input('height')
  height: number | undefined;
  @Input('deg')
  deg: number | undefined;
  @Input('enable')
  enable: boolean | undefined;
  @Output('onclick')
  onclickEvent = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  onclick(args: MouseEvent): void {
    this.onclickEvent.emit(this.tag);
  }
}
