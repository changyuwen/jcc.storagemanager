import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrowitemComponent } from './arrowitem.component';

describe('ArrowitemComponent', () => {
  let component: ArrowitemComponent;
  let fixture: ComponentFixture<ArrowitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrowitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrowitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
