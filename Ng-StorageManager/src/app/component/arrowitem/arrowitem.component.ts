import { Component, Input, OnInit } from '@angular/core';
import { IArrowOperations } from '../arrow/arrow.component';

export type Side = "Up" | "Down" | "Left" | "Right";
export interface IArrowItemOperation extends IArrowOperations {
  side: Side;
  distance: number;
}

@Component({
  selector: 'app-arrowItem',
  templateUrl: './arrowitem.component.html',
  styleUrls: ['./arrowitem.component.scss']
})
export class ArrowItemComponent implements OnInit, IArrowItemOperation {

  constructor() { }
  @Input('tag')
  tag?: string | undefined;
  @Input('width')
  width: number = 30;
  @Input('height')
  height: number = 30;
  @Input('deg')
  deg: number = 90;
  @Input('enable')
  enable: boolean = false;
  @Input('side')
  side: Side = "Up";
  @Input('distance')
  distance: number = 0;

  ngOnInit(): void {
  }

}
