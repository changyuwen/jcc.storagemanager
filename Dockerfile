FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
ARG NUGET_SOURCE
WORKDIR /src
# Copy everything else and build and restore as distinct layers
COPY ./ASP.NETCore.StorageManager/ ./
RUN ls -la ./
RUN dotnet publish ASP.NETCore.StorageManager/ASP.NETCore.StorageManager.csproj --source ${NUGET_SOURCE} -c Release  -o ../out

 #Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
EXPOSE 80 443
RUN cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
#RUN apt-get update && apt-get install -y libgdiplus
COPY --from=build /out ./
ADD ./ASP.NETCore.StorageManager/ASP.NETCore.StorageManager/ClientApp/ ./ClientApp/
#COPY --from=ng-build /usr/local/bin/dist/login ./ClientApp/dist/
ENTRYPOINT ["dotnet", "ASP.NETCore.StorageManager.dll"]