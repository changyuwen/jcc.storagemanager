﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace Test_Tool
{
    public partial class Form1 : Form
    {
        private JM.CallBack TFunction;
        public uint station;
        private string DestIP = "127.0.0.1";
        private string DestPort = "10001";
        public Form1()
        {
            InitializeComponent();

            
        } 
       

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                TFunction = CB;
                JM.DeInitialize();
                JM.Initialize();
                var port = ushort.Parse(txb_JMHostPort.Text);
                station = JM.StartStation(txb_JMHostName.Text, TFunction, 0, port);

              

                richTextBox1.Text += "Connecting...\n";

                //
                txb_JMIP.Enabled = true;
                txb_JMport.Enabled = true;
                

                //ToAddrInt(DestIP);
                //ushort.Parse(DestPort);
                DestIP = txb_JMIP.Text;
                DestPort = txb_JMport.Text;

                richTextBox1.Text += "Set Dest IP: " + DestIP + "\n";
                richTextBox1.Text += "Set Dest Port: " + DestPort + "\n";
                richTextBox1.Text += "Connect Success\n";

                txb_JMHostName.Enabled = false;
                txb_JMHostPort.Enabled = false;
                txb_JMIP.Enabled = false;
                txb_JMport.Enabled = false;
                button2.Enabled = false;

                //
            }
            catch(Exception ex)
            {
                richTextBox1.Text += ex.Message;
            }
           
        }

        private void CB(uint Station, uint type, uint ticket, ref byte[] s, uint length, uint sb, uint ip, ushort port)
        {

            //string convert = "This i\x00 the";

            // From string to byte array
            //byte[] buffer = Encoding..GetBytes(convert);

            // From byte array to string
            string p = Encoding.Default.GetString(s, 0, s.Length);


            string tt = "";



            switch (type)
            {
                case 0:
                    tt = JM.GetName(Station) + " message is delivered to " + ip + " port " + port + " for ticket = " + ticket + ", sb = " + sb;
                    break;

                case 1:
                    tt = JM.GetName(Station) + " failed to send message with ticket " + ticket;
                    break;

                case 2:
                    //Save received detail message

                    tt = JM.GetName(Station) + " <- Recv <" + p + "> From" + "<WEB UI>\n";
                    richTextBox1.Text += tt;
                    if (p.Equals("S1F1"))
                    {
                        JM.Send(Station, "S1F2", ip, port, ref ticket);
                        richTextBox1.Text += JM.GetName(station) + " -> Sent <S1F2> To <WEB UI>\n";
                    }
                    if (richTextBox1.Text.Length >= richTextBox1.MaxLength - 200)
                    {
                        richTextBox1.Clear();
                    }

                    break;

                case 3:
                    tt = JM.GetName(Station) + " request is received from " + ip + " port " + port + " with sb = " + sb + ", length = " + length;

                    break;

                case 4:
                    tt = JM.GetName(Station) + " reply is received from " + ip + " port " + port + " with sb = " + sb + ", length = " + length;
                    break;

                case 5:
                    tt = JM.GetName(Station) + " failed to receive reply from " + ip + " port " + port + ", ticket = " + ticket;
                    break;

                case 6:
                    tt = JM.GetName(Station) + " multi block receive failure";
                    break;
                case 7:
                    tt = JM.GetName(Station) + " One Shot timer expired : " + p + ", ticket = " + ticket;
                    break;

                case 8:
                    tt = JM.GetName(Station) + " Periodic timer expired : " + p + ", ticket = " + ticket;
                    for (int i = 0; i < 10; i++)
                    {


                        Thread.Sleep(1);
                    }
                    break;

            }
        }

        private static uint ToAddrInt(string addr)
        {
            var ipBytes = System.Net.IPAddress.Parse(addr).GetAddressBytes();
            var ip = (uint)ipBytes[3] << 24;
            ip += (uint)ipBytes[2] << 16;
            ip += (uint)ipBytes[1] << 8;
            ip += (uint)ipBytes[0];
            return ip;
        }

       


        private void txb_JMport_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                ((TextBox)sender).SelectAll();
            } 
        }

        private void txb_JMIP_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                ((TextBox)sender).SelectAll();
            } 
        }

        private void txb_JMHostPort_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                ((TextBox)sender).SelectAll();
            } 
        }

        private void txb_JMHostName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                ((TextBox)sender).SelectAll();
            } 
        }   
      


        private void button29_Click_1(object sender, EventArgs e)
        {
            uint ticket = 1;
            // Read the file and display it line by line.  
           
            try
            {
                foreach (string line in txb_EnterMessage.Lines)
                {
                    if (line.IndexOf("sleep") != -1 || line.IndexOf("Sleep") != -1)
                    {
                        string[] cmd = line.Split(' ');

                        Thread.Sleep(Convert.ToInt32(cmd[1]));
                    }
                    else
                    {
                        JM.Send(station, line , ToAddrInt(DestIP), ushort.Parse(DestPort), ref ticket);
                        richTextBox1.Text += JM.GetName(station) + " -> Sent <" + line + "> To " + "<" + ushort.Parse(DestPort) + ">\n";
                        txb_EnterMessage.Clear();
                    }
                }
            }
            catch
            {
            }
        }
        private void txb_enterMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button29.PerformClick();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uint ticket = 1;
            JM.Send(station, richTextBox2.Text, ToAddrInt(DestIP), ushort.Parse(DestPort), ref ticket);
            richTextBox2.Clear();
        }
    }
}
