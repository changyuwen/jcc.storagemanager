﻿namespace Test_Tool
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.txb_EnterMessage = new System.Windows.Forms.TextBox();
            this.button29 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txb_JMHostPort = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txb_JMHostName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txb_JMIP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txb_JMport = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(128, 18);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(314, 172);
            this.richTextBox1.TabIndex = 19;
            this.richTextBox1.Text = "";
            // 
            // txb_EnterMessage
            // 
            this.txb_EnterMessage.Location = new System.Drawing.Point(128, 199);
            this.txb_EnterMessage.Margin = new System.Windows.Forms.Padding(2);
            this.txb_EnterMessage.Name = "txb_EnterMessage";
            this.txb_EnterMessage.Size = new System.Drawing.Size(253, 22);
            this.txb_EnterMessage.TabIndex = 20;
            this.txb_EnterMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txb_enterMessage_KeyDown);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(385, 201);
            this.button29.Margin = new System.Windows.Forms.Padding(2);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(56, 18);
            this.button29.TabIndex = 21;
            this.button29.Text = "Send";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.txb_JMHostPort);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.txb_JMHostName);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txb_JMIP);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txb_JMport);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(113, 230);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(14, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Connect";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txb_JMHostPort
            // 
            this.txb_JMHostPort.AcceptsReturn = true;
            this.txb_JMHostPort.Location = new System.Drawing.Point(6, 75);
            this.txb_JMHostPort.Name = "txb_JMHostPort";
            this.txb_JMHostPort.Size = new System.Drawing.Size(100, 22);
            this.txb_JMHostPort.TabIndex = 17;
            this.txb_JMHostPort.Text = "5003";
            this.txb_JMHostPort.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txb_JMHostPort_KeyUp);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 59);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(24, 12);
            this.label25.TabIndex = 16;
            this.label25.Text = "Port";
            // 
            // txb_JMHostName
            // 
            this.txb_JMHostName.Location = new System.Drawing.Point(6, 33);
            this.txb_JMHostName.Name = "txb_JMHostName";
            this.txb_JMHostName.Size = new System.Drawing.Size(100, 22);
            this.txb_JMHostName.TabIndex = 14;
            this.txb_JMHostName.Text = "Joel";
            this.txb_JMHostName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txb_JMHostName_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "Name";
            // 
            // txb_JMIP
            // 
            this.txb_JMIP.Location = new System.Drawing.Point(6, 115);
            this.txb_JMIP.Name = "txb_JMIP";
            this.txb_JMIP.Size = new System.Drawing.Size(100, 22);
            this.txb_JMIP.TabIndex = 10;
            this.txb_JMIP.Text = "127.0.0.1";
            this.txb_JMIP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txb_JMIP_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "DIP";
            // 
            // txb_JMport
            // 
            this.txb_JMport.Location = new System.Drawing.Point(6, 158);
            this.txb_JMport.Name = "txb_JMport";
            this.txb_JMport.Size = new System.Drawing.Size(100, 22);
            this.txb_JMport.TabIndex = 12;
            this.txb_JMport.Text = "5002";
            this.txb_JMport.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txb_JMport_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 12);
            this.label3.TabIndex = 11;
            this.label3.Text = "DPort";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(128, 226);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(251, 96);
            this.richTextBox2.TabIndex = 22;
            this.richTextBox2.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(385, 299);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 389);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.txb_EnterMessage);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Train01";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.TextBox txb_EnterMessage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txb_JMHostPort;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txb_JMHostName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txb_JMIP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txb_JMport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button1;
    }
}

